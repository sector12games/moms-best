﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace MomsBest.Models
{
    public class Recipe
    {
        [Key]
        public int Id { get; set; }

        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }

        [Required]
        public string Name { get; set; }

        public int? PrepTimeHours { get; set; }
        public int? PrepTimeMinutes { get; set; }

        public string Directions { get; set; }

        [Required]
        public string PictureUrl { get; set; }

        public DateTime? DateCreated { get; set; }

        public virtual ICollection<Ingredient> Ingredients { get; set; }
    }
}