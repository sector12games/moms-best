﻿using System.Data.Entity;

namespace MomsBest.Models
{
    public class MomsBestDbContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
    }
}