﻿using System.Data.Entity;

namespace MomsBest.Models
{
    public class SeedData : DropCreateDatabaseIfModelChanges<MomsBestDbContext>
    {
        protected override void Seed(MomsBestDbContext context)
        {
            // Create categories
            Category cookies = context.Categories.Add(new Category() { Name = "Cookies"});
            Category browniesAndBars = context.Categories.Add(new Category() { Name = "Brownies & Bars" });
            Category pies = context.Categories.Add(new Category() { Name = "Pies" });
            Category cakesAndCupcakes = context.Categories.Add(new Category() { Name = "Cakes & Cupcakes" });
            Category maltsAndShakes = context.Categories.Add(new Category() { Name = "Malts & Shakes" });
            Category drinks = context.Categories.Add(new Category() { Name = "Drinks" });

            // Create recipes
            Recipe chocolateChip = context.Recipes.Add(new Recipe()
            {
                Name = "Chocolate Chip",
                Category = cookies,
                PictureUrl = "http://sookiescookies.com/wp-content/uploads/2012/01/Chocolate_chip_cookies.jpg"
            });
            Recipe peanutButter = context.Recipes.Add(new Recipe()
            {
                Name = "Peanut Butter",
                Category = cookies,
                PictureUrl = "http://www.chellascommoncents.com/sites/default/files/images/recipes/peanut_butter_cookies.jpg"
            });
            Recipe oatmeal = context.Recipes.Add(new Recipe()
            {
                Name = "Oatmeal",
                Category = cookies,
                PictureUrl = "http://www.thevirtualcake.com/thumbs/1344009-OatmealTrailMixCookieforOurVintageRecipeSwap.jpg"
            });

            Recipe stickyChocoloateBrownies = context.Recipes.Add(new Recipe()
            {
                Name = "Sticky Chocolate Brownies",
                Category = browniesAndBars,
                PictureUrl = "https://d2k9njawademcf.cloudfront.net/indeximages/19598/original/brownies1.jpg?1403258350"
            });
            Recipe whiteChocolateBrownies = context.Recipes.Add(new Recipe()
            {
                Name = "White Chocolate Brownies",
                Category = browniesAndBars,
                PictureUrl = "http://tennesseesugar.com/wp-content/uploads/2011/08/brownies-with-warm-white-chocolate-sauce02-fg.jpg"
            });
            Recipe superLemonyLemonBars = context.Recipes.Add(new Recipe()
            {
                Name = "Super Lemony Lemon Bars",
                Category = browniesAndBars,
                PictureUrl = "http://cookinfood.com/wp-content/uploads/2012/02/lemon-bars-312dff.png?w=1024"
            });

            Recipe perfectApplePie = context.Recipes.Add(new Recipe()
            {
                Name = "Perfect Apple Pie",
                Category = pies,
                PictureUrl = "http://s3.amazonaws.com/gmi-digital-library/3cf7cb34-8f77-4885-bfec-697f96dd5d61.jpg"
            });
            Recipe strawberryRhubarbPie = context.Recipes.Add(new Recipe()
            {
                Name = "Super Lemony Lemon Bars",
                Category = pies,
                PictureUrl = "http://www.landolakes.com/assets/images/recipe/orig/13911.jpg"
            });
            Recipe chocolatePie = context.Recipes.Add(new Recipe()
            {
                Name = "Chocolate Pie",
                Category = pies,
                PictureUrl = "http://cdn.instructables.com/FGE/ZM98/G29ZFJ05/FGEZM98G29ZFJ05.LARGE.jpg"
            });

            Recipe strawberryLemonadeCupcakes = context.Recipes.Add(new Recipe()
            {
                Name = "Strawberry Lemonade Cupcakes",
                Category = cakesAndCupcakes,
                PictureUrl = "http://easybaked.files.wordpress.com/2013/03/strawberry-lemonade-cupcake2.jpg"
            });
            Recipe redVelvetCake = context.Recipes.Add(new Recipe()
            {
                Name = "Kate's Red Velvet Cake",
                Category = cakesAndCupcakes,
                PictureUrl = "http://s3.amazonaws.com/twduncan/recipes/1400219467.73_CherryRedVelvetCake_slice_2950_raw.jpg"
            });
            Recipe cheesecake = context.Recipes.Add(new Recipe()
            {
                Name = "Traditional Cheesecake",
                Category = cakesAndCupcakes,
                PictureUrl = "http://reciperhapsody.files.wordpress.com/2011/04/ultimate-cheesecake-3-31-11-3.jpg"
            });

            Recipe chocolateMalt = context.Recipes.Add(new Recipe()
            {
                Name = "Chocolate Malt",
                Category = maltsAndShakes,
                PictureUrl = "http://upload.wikimedia.org/wikipedia/commons/7/70/Chocolate_Malt_with_Sprinkles.jpg"
            });
            Recipe saltedMochaCaramelShake = context.Recipes.Add(new Recipe()
            {
                Name = "Salted Mocha Caramel Shake",
                Category = maltsAndShakes,
                PictureUrl = "http://theplungeproject.com/wp-content/uploads/2011/12/saltedcaramel.jpg"
            });
            Recipe classicVanillaMilkshake = context.Recipes.Add(new Recipe()
            {
                Name = "Traditional Cheesecake",
                Category = maltsAndShakes,
                PictureUrl = "http://d28h4ey7qefd3m.cloudfront.net/wp-content/uploads/2010/12/Vanilla_Milkshake.jpg"
            });

            Recipe momsHomemadeHotChocolate = context.Recipes.Add(new Recipe()
            {
                Name = "Mom's Homemade Hot Chocolate",
                Category = drinks,
                PictureUrl = "http://charlottesyogurt.com/wp-content/uploads//2014/05/Pirouline-Hot-Chocolate-Cocktail.jpg"
            });
            Recipe christmasMulledWine = context.Recipes.Add(new Recipe()
            {
                Name = "Christmas Mulled Wine",
                Category = drinks,
                PictureUrl = "http://tastygorgeous.com/wp-content/uploads/2013/12/LizEarle_MavellousMulledWine.jpg"
            });
            Recipe momsHomemadeEggNog = context.Recipes.Add(new Recipe()
            {
                Name = "Mom's Homemade Egg Nog",
                Category = drinks,
                PictureUrl = "http://tastykitchen.com/wp-content/uploads/2012/12/TK-Blog-Eggnog.jpg"
            });
        }
    }
}