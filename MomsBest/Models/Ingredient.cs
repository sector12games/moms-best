﻿using System.ComponentModel.DataAnnotations;

namespace MomsBest.Models
{
    public class Ingredient
    {
        [Key]
        public int Id { get; set; }

        public int RecipeId { get; set; }
        public virtual Recipe Recipe { get; set; }

        public string Name { get; set; }
        public string Quantity { get; set; }
    }
}