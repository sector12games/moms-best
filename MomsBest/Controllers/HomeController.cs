﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MomsBest.Models;

namespace MomsBest.Controllers
{
    public class HomeController : Controller
    {
        private MomsBestDbContext db = new MomsBestDbContext();

        public ActionResult Index()
        {
            List<Recipe> recipes = db.Recipes.OrderBy(x => x.DateCreated).ToList();
            return View(recipes);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult RecipesInCategory(int id)
        {
            // Attempt to find the category provided
            Category category = db.Categories.FirstOrDefault(x => x.Id == id);
            return View(category);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}