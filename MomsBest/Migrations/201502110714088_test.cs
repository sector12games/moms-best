namespace MomsBest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Ingredients", "Name", c => c.String());
            AlterColumn("dbo.Ingredients", "Quantity", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Ingredients", "Quantity", c => c.String(nullable: false));
            AlterColumn("dbo.Ingredients", "Name", c => c.String(nullable: false));
        }
    }
}
