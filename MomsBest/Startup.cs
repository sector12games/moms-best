﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MomsBest.Startup))]
namespace MomsBest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
